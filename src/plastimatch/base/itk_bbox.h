/* -----------------------------------------------------------------------
   See COPYRIGHT.TXT and LICENSE.TXT for copyright and license information
   ----------------------------------------------------------------------- */
#ifndef _itk_bbox_h_
#define _itk_bbox_h_

#include "plmbase_config.h"
#include "itk_image_type.h"

void
itk_bbox (const UCharImageType::Pointer img, float *bbox);

#endif
